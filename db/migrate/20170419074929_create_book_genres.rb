class CreateBookGenres < ActiveRecord::Migration[5.0]
  def change
    create_table :book_genres do |t|
      t.integer :book_id, index: true, nil: false
      t.integer :genre_id, index: true, nil: false

      t.timestamps
    end

    add_foreign_key :book_genres, :books
    add_foreign_key :book_genres, :genres
  end
end
