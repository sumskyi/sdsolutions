class CreateArticleAuthors < ActiveRecord::Migration[5.0]
  def change
    create_table :article_authors do |t|
      t.integer :article_id, index: true, nil: false
      t.integer :author_id, index: true, nil: false
      t.timestamps
    end

    add_foreign_key :article_authors, :articles
    add_foreign_key :article_authors, :authors
  end
end
