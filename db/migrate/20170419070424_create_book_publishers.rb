class CreateBookPublishers < ActiveRecord::Migration[5.0]
  def change
    create_table :book_publishers do |t|
      t.integer :book_id, index: true, nil: false
      t.integer :publisher_id, index: true, nil: false
      t.timestamps
    end

    add_foreign_key :book_publishers, :books
    add_foreign_key :book_publishers, :publishers
  end
end
