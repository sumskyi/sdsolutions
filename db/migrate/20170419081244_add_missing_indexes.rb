class AddMissingIndexes < ActiveRecord::Migration[5.0]
  def change
    add_index :authors, :name
    add_index :genres, :name
    add_index :publishers, :name
  end
end
