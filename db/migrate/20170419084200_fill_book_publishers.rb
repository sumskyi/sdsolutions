class FillBookPublishers < ActiveRecord::Migration[5.0]
  def change
    drop_table :book_publishers
    add_column :books, :publisher_id, :integer, index: true
    add_foreign_key :books, :publishers

    Book.find_each do |book|
      publisher = Publisher.find_or_create_by(name: book.read_attribute(:publisher))

      book.publisher = publisher
      book.save
    end

    remove_column :books, :publisher
  end
end
