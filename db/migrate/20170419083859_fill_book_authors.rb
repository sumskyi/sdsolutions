class FillBookAuthors < ActiveRecord::Migration[5.0]
  def change
    Book.find_each do |book|
      authors = MigrationHelper.clean_names(book.read_attribute(:authors))

      authors.each do |name|
        author = Author.find_or_create_by(name: name)

        book.authors << author
      end
    end

    remove_column :books, :authors
  end
end
