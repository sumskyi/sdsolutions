class FillBookGenres < ActiveRecord::Migration[5.0]
  def change
    Book.find_each do |book|
      genres = MigrationHelper.clean_names(book.read_attribute(:genres))

      genres.each do |name|
        genre = Genre.find_or_create_by(name: name)

        book.genres << genre
      end
    end

    remove_column :books, :genres
  end
end
