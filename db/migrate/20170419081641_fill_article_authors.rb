class FillArticleAuthors < ActiveRecord::Migration[5.0]
  def change
    Article.find_each do |article|
      authors = MigrationHelper.clean_names(article.read_attribute(:authors))

      authors.each do |name|
        author = Author.find_or_create_by(name: name)

        article.authors << author
      end
    end

    remove_column :articles, :authors
  end
end
