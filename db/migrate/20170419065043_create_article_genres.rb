class CreateArticleGenres < ActiveRecord::Migration[5.0]
  def change
    create_table :article_genres do |t|
      t.integer :article_id, index: true, nil: false
      t.integer :genre_id, index: true, nil: false
      t.timestamps
    end

    add_foreign_key :article_genres, :articles
    add_foreign_key :article_genres, :genres
  end
end
