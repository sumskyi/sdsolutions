class AddIndexOnNameLengthToBooks < ActiveRecord::Migration[5.0]
  def change
    add_index :books, '(LENGTH(name))'
  end
end
