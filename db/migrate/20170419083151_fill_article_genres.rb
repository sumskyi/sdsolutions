class FillArticleGenres < ActiveRecord::Migration[5.0]
  def change
    Article.find_each do |article|
      genres = MigrationHelper.clean_names(article.read_attribute(:genres))

      genres.each do |name|
        genre = Genre.find_or_create_by(name: name)

        article.genres << genre
      end
    end

    remove_column :articles, :genres
  end
end
