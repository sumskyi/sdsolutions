class CreateBookAuthors < ActiveRecord::Migration[5.0]
  def change
    create_table :book_authors do |t|
      t.integer :book_id, index: true, nil: false
      t.integer :author_id, index: true, nil: false

      t.timestamps
    end

    add_foreign_key :book_authors, :books
    add_foreign_key :book_authors, :authors
  end
end
