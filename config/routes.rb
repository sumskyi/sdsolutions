Rails.application.routes.draw do
  resources :articles, only: [:index]

  resources :books, only: [:index] do
    collection do
      get :prime
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
