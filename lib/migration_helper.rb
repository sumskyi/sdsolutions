module MigrationHelper
  module_function

  def clean_names(names)
    names.split(/,/).map(&:strip)
  end
end
