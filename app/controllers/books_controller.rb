class BooksController < ApplicationController
  def index
    @books = Book.all.includes(:authors, :genres)
  end

  def prime
    @books = Book.prime_names.includes(:authors, :genres)
    render :index
  end
end
