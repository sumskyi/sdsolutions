class Genre < ApplicationRecord
  has_many :article_genres
  has_many :articles, through: :article_genres

  has_many :book_genres
  has_many :books, through: :book_genres

  def to_s
    name
  end
end
