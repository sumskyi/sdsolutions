class Author < ApplicationRecord
  has_many :article_authors
  has_many :articles, through: :article_authors

  has_many :book_authors
  has_many :books, through: :book_authors

  def to_s
    name
  end
end
