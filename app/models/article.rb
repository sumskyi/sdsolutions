class Article < ApplicationRecord
  has_many :article_authors
  has_many :authors, through: :article_authors

  has_many :article_genres
  has_many :genres, through: :article_genres
end
