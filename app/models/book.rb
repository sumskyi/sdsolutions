class Book < ApplicationRecord
  belongs_to :publisher

  has_many :book_genres
  has_many :genres, through: :book_genres

  has_many :book_authors
  has_many :authors, through: :book_authors

  PRIME_NUMBERS = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
                   59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113,
                   127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181,
                   191, 193, 197, 199]

  scope :prime_names, -> {
    where("LENGTH(name) IN (%s)", PRIME_NUMBERS.join(','))
  }
end
